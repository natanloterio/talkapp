package br.com.radioindoor.talkapp.service;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;

public interface TalkAppWebService {

    @FormUrlEncoded
    @POST("talkapp")
    Call<User> logginUser(@Field("user") String first, @Field("pass") String last);


    @PUT("talkapp")
    Call<String> uploadAttachment(@Body RequestBody thumbnailContent);


}
