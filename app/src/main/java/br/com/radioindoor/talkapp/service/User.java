package br.com.radioindoor.talkapp.service;

public class User {

    //{"user":"andre","pass":null,"aid":"aid123456789","titulo":"R\u00e1dio Andr\u00e9","local":"Blumenau, SC","key":"5d61d8ad8918db082daa12cb1e7eb221"}

    public String user;
    public String pass;
    public String aid;
    public String titulo;
    public String local;
    public String key;
}
