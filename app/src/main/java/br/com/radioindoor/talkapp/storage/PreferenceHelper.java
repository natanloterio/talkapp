package br.com.radioindoor.talkapp.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.radioindoor.talkapp.NewRecordActivity;
import br.com.radioindoor.talkapp.service.User;

public class PreferenceHelper {

    private static final String USER_CONST = "user";

    public static void saveLoggedUser(Context context, User user){
        SharedPreferences sharedPref = context.getApplicationContext().getSharedPreferences(USER_CONST, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        editor.putString(USER_CONST, gson.toJson(user));
        editor.apply();
    }

    public static User getLoggedUser(Context context){
        SharedPreferences sharedPref = context.getApplicationContext().getSharedPreferences(USER_CONST, Context.MODE_PRIVATE);
        String strUser = sharedPref.getString(USER_CONST,null);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        if(strUser != null && !strUser.isEmpty()){
            return gson.fromJson(strUser,User.class);
        }else{
            return null;
        }
    }

    public static String getAID(Context context) {
        User loggedUser = getLoggedUser(context);
        if( loggedUser != null){
            return loggedUser.aid;
        }else{
            return "";
        }
    }

    public static String getKEY(Context context) {
        User loggedUser = getLoggedUser(context);
        if( loggedUser != null){
            return loggedUser.key;
        }else{
            return "";
        }
    }

    public static void clearPreffs(Context context) {
        SharedPreferences sharedPref = context.getApplicationContext().getSharedPreferences(USER_CONST, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();

    }
}
