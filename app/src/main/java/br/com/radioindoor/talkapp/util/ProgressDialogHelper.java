package br.com.radioindoor.talkapp.util;

import android.app.ProgressDialog;
import android.content.Context;

public class ProgressDialogHelper {

    public static ProgressDialog showDialogProgress(Context context, String message){
        ProgressDialog progressDoalog = new ProgressDialog(context);
        progressDoalog.setIndeterminate(true);
        progressDoalog.setMessage(message);
        //progressDoalog.setTitle("ProgressDialog bar example");
        progressDoalog.show();
        return progressDoalog;
    }
}
