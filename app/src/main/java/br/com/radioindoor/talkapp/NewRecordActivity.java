package br.com.radioindoor.talkapp;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaRecorder;

import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.TimerTask;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import android.support.v4.app.ActivityCompat;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.radioindoor.talkapp.service.TalkAppWebService;
import br.com.radioindoor.talkapp.service.User;
import br.com.radioindoor.talkapp.storage.PreferenceHelper;
import br.com.radioindoor.talkapp.util.ProgressDialogHelper;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewRecordActivity extends AppCompatActivity {


    private static final String FILETYPE = "aac";
    View buttonStartRecording, buttonStopRecording, buttonPlayLastRecordAudio,
            buttonStopPlayingRecording,buttonEnviar ;
    TextView txtTimer, txtCidade, txtRadio;
    String AudioSavePathInDevice = null;
    MediaRecorder mediaRecorder ;
    Random random ;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    public static final int RequestPermissionCode = 1;
    MediaPlayer mediaPlayer ;
    private TalkAppWebService service;
    private View linlayPlayLastRecord;
    private boolean isRunning;
    private Handler mHandler;
    private long mStartTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_record_new);

        buttonStartRecording = (View) findViewById(R.id.activity_new_record_n_btn_iniciar_gravacao);
        buttonStopRecording = (View) findViewById(R.id.activity_new_record_n_btn_parar_gravacao);
        buttonPlayLastRecordAudio = (View) findViewById(R.id.activity_new_record_n_btn_ouvir);
        buttonStopPlayingRecording = (View)findViewById(R.id.activity_new_record_n_btn_parar_ouvir);
        buttonEnviar = (View)findViewById(R.id.activity_new_record_n_btn_enviar);
        txtTimer = (TextView) findViewById(R.id.activity_new_record_n_txt_timer);
        txtRadio = (TextView) findViewById(R.id.activity_new_record_n_titulo_radio);
        txtCidade = (TextView) findViewById(R.id.activity_new_record_n_cidade_radio);

        linlayPlayLastRecord = findViewById(R.id.activity_new_record_n_linlay_last_record);
        linlayPlayLastRecord.setVisibility(View.INVISIBLE);
        buttonStopRecording.setVisibility(View.GONE);
        //buttonPlayLastRecordAudio.setVisibility(View.GONE);
        //buttonStopPlayingRecording.setVisibility(View.GONE);
        User user = PreferenceHelper.getLoggedUser(this);
        txtRadio.setText(user.titulo);
        txtCidade.setText(user.local);

        random = new Random();

        buttonStartRecording.setOnClickListener(onClickStartRecording());

        buttonStopRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaRecorder.stop();
                buttonStopRecording.setVisibility(View.GONE);
                linlayPlayLastRecord.setVisibility(View.VISIBLE);
//                buttonPlayLastRecordAudio.setVisibility(View.VISIBLE);
                buttonStartRecording.setVisibility(View.VISIBLE);
                buttonStopPlayingRecording.setVisibility(View.GONE);
                pararTimer();

            }
        });

        buttonPlayLastRecordAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws IllegalArgumentException,
                    SecurityException, IllegalStateException {

                buttonStopRecording.setEnabled(false);
                buttonStartRecording.setEnabled(false);
                buttonPlayLastRecordAudio.setVisibility(View.GONE);
                buttonStopPlayingRecording.setVisibility(View.VISIBLE);

                mediaPlayer = new MediaPlayer();
                try {
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                            buttonStopPlayingRecording.setVisibility(View.GONE);
                            buttonPlayLastRecordAudio.setVisibility(View.VISIBLE);
                            buttonStopRecording.setEnabled(true);
                            buttonStartRecording.setEnabled(true);
                        }
                    });
                    mediaPlayer.setDataSource(AudioSavePathInDevice);
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mediaPlayer.start();
                /*Toast.makeText(NewRecordActivity.this, "Recording Playing",
                        Toast.LENGTH_LONG).show();*/
            }
        });

        buttonStopPlayingRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonStopRecording.setEnabled(true);
                buttonStartRecording.setEnabled(true);
                buttonStopPlayingRecording.setVisibility(View.GONE);
                buttonPlayLastRecordAudio.setVisibility(View.VISIBLE);

                if(mediaPlayer != null){
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    MediaRecorderReady();
                }
            }
        });

        buttonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                File file = new File(AudioSavePathInDevice);// initialize file here

                InputStream in = null;
                try {
                    in = new FileInputStream(file);

                    byte[] buf;
                    buf = new byte[in.available()];
                    while (in.read(buf) != -1);
                    RequestBody requestBody = RequestBody
                            .create(MediaType.parse("application/octet-stream"), buf);

                    Call<String> call = getService().uploadAttachment(requestBody);
                    final ProgressDialog dialog = ProgressDialogHelper.showDialogProgress(NewRecordActivity.this, "Enviando...");
                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            Toast.makeText(NewRecordActivity.this,"Audio enviado",Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Toast.makeText(NewRecordActivity.this,"Ocorreu um problema. Tente novamente mais tarde",Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        }
                    });
                    } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private View.OnClickListener onClickStartRecording() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(checkPermission()) {

                    AudioSavePathInDevice =
                            Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                                    CreateRandomAudioFileName(5) + "AudioRecording."+FILETYPE;

                    MediaRecorderReady();

                    try {
                        mediaRecorder.prepare();
                        mediaRecorder.start();
                    } catch (IllegalStateException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    buttonStartRecording.setVisibility(View.GONE);
                    buttonStopRecording.setVisibility(View.VISIBLE);

                    iniciarTimer();

                } else {
                    requestPermission();
                }

            }
        };
    }

    private void iniciarTimer() {
        mHandler = new Handler();
        isRunning = true;
        mStartTime = System.currentTimeMillis();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(isRunning) {
                    exibirDuracao();
                    mHandler.postDelayed(this,1000);
                }
            }
        },1000);

    }

    private void pararTimer(){
        isRunning = false;
    }

    private void exibirDuracao() {
        long millis = System.currentTimeMillis() - mStartTime;
        int seconds = (int) (millis / 1000);
        final int minutes = seconds / 60;
        seconds     = seconds % 60;

        final int finalSeconds = seconds;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txtTimer.setText(String.format("%d:%02d", minutes, finalSeconds));
            }
        });
    }

    private TalkAppWebService getService(){
        if(service == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
            clientBuilder.
                    addInterceptor(new Interceptor() {
                        @Override
                        public okhttp3.Response intercept(Chain chain) throws IOException {
                            Request original = chain.request();
                            Request.Builder requestBuilder = original.newBuilder();
                            requestBuilder.header("upload-name", getFileName());
                            requestBuilder.header("upload-type",FILETYPE);
                            requestBuilder.header("upload-size", getFileSize());
                            requestBuilder.header("upload-aid", PreferenceHelper.getAID(NewRecordActivity.this));
                            requestBuilder.header("upload-key", PreferenceHelper.getKEY(NewRecordActivity.this));


                            Request request = requestBuilder.build();
                            return chain.proceed(request);
                        }
                    });
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://radioindoor.com.br/api/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(clientBuilder.build())
                    .build();

            service = retrofit.create(TalkAppWebService.class);
        }
        return service;
    }

    public void MediaRecorderReady(){
        mediaRecorder=new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        //mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mediaRecorder.setAudioSamplingRate(44100);
        mediaRecorder.setAudioEncodingBitRate(96000);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    public String CreateRandomAudioFileName(int string){
        StringBuilder stringBuilder = new StringBuilder( string );
        int i = 0 ;
        while(i < string ) {
            stringBuilder.append(RandomAudioFileName.
                    charAt(random.nextInt(RandomAudioFileName.length())));

            i++ ;
        }
        return stringBuilder.toString();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(NewRecordActivity.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length> 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {

                    } else {
                        Toast.makeText(NewRecordActivity.this,"Voce precisar dar permissao",Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(NewRecordActivity.this,R.style.AlertDialogCustom);
        builder.setMessage("Deseja sair?");
        builder.setTitle("Atençao");

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
                PreferenceHelper.clearPreffs(NewRecordActivity.this);
            }
        });
        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(isRunning){
            onClickStartRecording().onClick(null);
        }
    }

    public String getFileName() {
        return AudioSavePathInDevice != null?AudioSavePathInDevice:"";
    }

    public String getFileSize() {
        return new File(AudioSavePathInDevice).length()+"";
    }
}
